// *****************************************************************************
// Projects
// *****************************************************************************

addCommandAlias("check", "compile:scalafix --check; test:scalafix --check")
addCommandAlias("fix", "compile:scalafix; test:scalafix; scalafmtSbt; scalafmtAll")

lazy val root =
  project
    .in(file("."))
    .aggregate(countries.js, countries.jvm)
    .settings(commonSettings)
    .settings(
      crossScalaVersions := Nil, // Workaround for "Repository for publishing is not specified." error
      publish := {},
      publishLocal := {},
    )

lazy val countries =
  crossProject(JSPlatform, JVMPlatform)
    .in(file("."))
    .enablePlugins(AutomateHeaderPlugin)
    .settings(commonSettings)
    .settings(publishSettings)
    .settings(scalafmtSettings)
    .settings(scoverageSettings)
    .settings(
      libraryDependencies ++= Seq(
        "org.typelevel"     %%% "cats-core"          % Version.cats          % Provided,
        "io.circe"          %%% "circe-core"         % Version.circe         % Provided,
        "io.circe"          %%% "circe-generic"      % Version.circe         % Provided,
        "io.circe"          %%% "circe-refined"      % Version.circe         % Provided,
        "io.circe"          %%% "circe-parser"       % Version.circe         % Provided,
        "com.beachape"      %%% "enumeratum"         % Version.enumeratum    % Provided,
        "com.beachape"      %%% "enumeratum-cats"    % Version.enumeratum    % Provided,
        "com.beachape"      %%% "enumeratum-circe"   % Version.enumeratum    % Provided,
        "eu.timepit"        %%% "refined"            % Version.refined       % Provided,
        "eu.timepit"        %%% "refined-cats"       % Version.refined       % Provided,
        "eu.timepit"        %%% "refined-scalacheck" % Version.refined       % Test,
        "org.scalacheck"    %%% "scalacheck"         % Version.scalaCheck    % Test,
        "org.scalatest"     %%% "scalatest"          % Version.scalaTest     % Test,
        "org.scalatestplus" %%% "scalacheck-1-15"    % Version.scalaTestPlus % Test
      )
    )
    .jsSettings(
      coverageEnabled := false,
      mimaFailOnNoPrevious := false,
      scalaJSUseMainModuleInitializer := false,
      Test / scalaJSUseMainModuleInitializer := false
    )
    .jvmSettings(
      libraryDependencies += "org.scala-js" %% "scalajs-stubs" % "1.1.0" % Provided,
      mimaPreviousArtifacts := Set("com.wegtam" %% "countries" % "1.0.0")
    )

// *****************************************************************************
// Library dependencies
// *****************************************************************************

lazy val Version = new {
  val cats          = "2.7.0"
  val circe         = "0.14.1"
  val enumeratum    = "1.7.0"
  val refined       = "0.9.28"
  val scalaCheck    = "1.15.4"
  val scalaTest     = "3.2.11"
  val scalaTestPlus = "3.2.11.0"
}

// *****************************************************************************
// Settings
// *****************************************************************************

def compilerSettings(sv: String) =
  CrossVersion.partialVersion(sv) match {
    case Some((2, 12)) =>
      Seq(
      "-deprecation",
      "-encoding", "UTF-8",
      "-explaintypes",
      "-feature",
      "-language:higherKinds",
      "-unchecked",
      "-Xcheckinit",
      "-Xfatal-warnings",
      "-Xfuture",
      "-Xlint:adapted-args",
      "-Xlint:by-name-right-associative",
      "-Xlint:constant",
      "-Xlint:delayedinit-select",
      "-Xlint:doc-detached",
      "-Xlint:inaccessible",
      "-Xlint:infer-any",
      "-Xlint:missing-interpolator",
      "-Xlint:nullary-override",
      "-Xlint:nullary-unit",
      "-Xlint:option-implicit",
      "-Xlint:package-object-classes",
      "-Xlint:poly-implicit-overload",
      "-Xlint:private-shadow",
      "-Xlint:stars-align",
      "-Xlint:type-parameter-shadow",
      "-Xlint:unsound-match",
      "-Ydelambdafy:method",
      "-Yno-adapted-args",
      "-Ypartial-unification",
      "-Ywarn-numeric-widen",
      "-Ywarn-unused-import",
      "-Ywarn-value-discard"
    )
    case _ =>
      Seq(
      "-deprecation",
      "-explaintypes",
      "-feature",
      "-language:higherKinds",
      "-unchecked",
      "-Xcheckinit",
      "-Xfatal-warnings",
      "-Xlint:adapted-args",
      "-Xlint:constant",
      "-Xlint:delayedinit-select",
      "-Xlint:doc-detached",
      "-Xlint:inaccessible",
      "-Xlint:infer-any",
      "-Xlint:missing-interpolator",
      "-Xlint:nullary-unit",
      "-Xlint:option-implicit",
      "-Xlint:package-object-classes",
      "-Xlint:poly-implicit-overload",
      "-Xlint:private-shadow",
      "-Xlint:stars-align",
      "-Xlint:type-parameter-shadow",
      "-Ywarn-dead-code",
      "-Ywarn-extra-implicit",
      "-Ywarn-numeric-widen",
      "-Ywarn-unused:implicits",
      "-Ywarn-unused:imports",
      "-Ywarn-unused:locals",
      "-Ywarn-unused:params",
      "-Ywarn-unused:patvars",
      "-Ywarn-unused:privates",
      "-Ywarn-value-discard",
      "-Ycache-plugin-class-loader:last-modified",
      "-Ycache-macro-class-loader:last-modified",
      "-Ymacro-annotations"
    )
  }


lazy val commonSettings =
  Seq(
    scalaVersion := "2.13.8",
    crossScalaVersions := Seq(scalaVersion.value, "2.12.15"),
    organization := "com.wegtam",
    organizationName := "Wegtam GmbH",
    organizationHomepage := Some(url("https://www.wegtam.com")),
    startYear := Some(2020),
    licenses += ("MPL-2.0", url("https://www.mozilla.org/en-US/MPL/2.0/")),
    addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.1"),
    addCompilerPlugin("org.typelevel" %% "kind-projector"     % "0.10.3"),
    scalacOptions ++= compilerSettings(scalaVersion.value),
    Compile / console / scalacOptions --= Seq(
      "-Xfatal-warnings",
      "-Ywarn-unused-import",
      "-Ywarn-unused:implicits",
      "-Ywarn-unused:imports",
      "-Ywarn-unused:locals",
      "-Ywarn-unused:params",
      "-Ywarn-unused:patvars",
      "-Ywarn-unused:privates"
    ),
    Compile / compile / wartremoverWarnings ++= Warts.unsafe,
    resolvers ++= Seq(
      Resolver.sonatypeRepo("releases"),
      Resolver.sonatypeRepo("snapshots")
    )
  )

lazy val publishSettings =
  Seq(
    credentials += Credentials(Path.userHome / ".sbt" / "sonatype_credentials"),
    description := "This library simply provides a list of countries as data models.",
    developers := List(
      Developer(
        "wegtam",
        "Wegtam GmbH",
        "tech@wegtam.com",
        url("https://www.wegtam.com")
      ),
      Developer(
        "jan0sch",
        "Jens Grassel",
        "jens@wegtam.com",
        url("https://www.jan0sch.de")
      )
    ),
    ThisBuild / dynverSeparator := "-",
    ThisBuild / dynverSonatypeSnapshots := true,
    homepage := Option(url("https://codeberg.org/wegtam/countries")),
    pgpSigningKey := Option(sys.env.getOrElse("PUBLISH_SIGNING_KEY", "633EA119CC2D5F249A0F409A002841124A42559E")),
    pomIncludeRepository := (_ => false),
    Test / publishArtifact := false,
    publishMavenStyle := true,
    publishTo := {
      val nexus = "https://oss.sonatype.org/"
      if (isSnapshot.value)
        Some("snapshots".at(nexus + "content/repositories/snapshots"))
      else
        Some("releases".at(nexus + "service/local/staging/deploy/maven2"))
    },
    publish := (publish dependsOn (Test / test)).value,
    scmInfo := Option(ScmInfo(
      url("https://codeberg.org/wegtam/countries"),
      "git@codeberg.org:wegtam/countries.git"
    )),
    ThisBuild / versionScheme := Some("semver-spec")
  )

lazy val scalafmtSettings =
  Seq(
    scalafmtOnCompile := true,
  )

lazy val scoverageSettings =
  Seq(
    coverageMinimumStmtTotal := 60,
    coverageFailOnMinimum := false,
    coverageHighlighting := true
  )
