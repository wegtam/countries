# A list of countries. #

![Sonatype Nexus (Releases)](https://img.shields.io/nexus/r/com.wegtam/countries_2.13?server=https%3A%2F%2Foss.sonatype.org)

This library simply provides a list of countries as data models.

In case you need a basic country model or simply a list which should contain
most (if not all) countries then we got you covered.

The basic model looks like this:

```scala
sealed abstract class Country(
  val alpha2: Alpha2CountryCode,
  val alpha3: Alpha3CountryCode,
  val name: CountryName,
  val nameOfficial: StateName,
  val numeric: NumericCountryCode,
  val tld: List[TopLevelDomain]
)
```

## Dependencies

The following libraries are used and expected to be provided by your project:

- Cats
- Circe
- Enumeratum
- Refined

## Examples

```text
scala> import com.wegtam.countries._, eu.timepit.refined.auto._
scala> Country.values.headOption
res0: Option[com.wegtam.countries.Country] = Some(Afghanistan)
scala> Country.fromAlpha2("DE")
res1: Either[String,com.wegtam.countries.Country] = Right(Germany)
scala> Country.fromAlpha2("XX")
res2: Either[String,com.wegtam.countries.Country] = Left(No such country!)
scala> Country.fromAlpha3("ESH")
res3: Either[String,com.wegtam.countries.Country] = Right(WesternSahara)
scala> Country.fromNumeric("858")
res4: Either[String,com.wegtam.countries.Country] = Right(Uruguay)
scala> import codecs.circe.alpha2._, io.circe.syntax._
scala> Country.fromNumeric("858").map(_.asJson)
res5: scala.util.Either[String,io.circe.Json] = Right("UY")
```

## Circe Codecs

Because quite often you need JSON, we provide several codecs. Please import the
ones that you prefer. Currently three codecs are provided:

1. decode / encode from / to the alpha-2 country code (`codecs.circe.alpha2`)
2. decode / encode from / to the alpha-3 country code (`codecs.circe.alpha3`)
3. decode / encode from / to the numeric country code (`codecs.circe.numeric`)

Please note that the "numeric" encoder will still produce strings because there
are codec with leading zeros!

## Getting started

The artefacts should be available from maven central, so just add the
dependency to your build configuration.

```
libraryDependencies += "com.wegtam" %% "countries" % VERSION
```

For development and testing you need to install [sbt](http://www.scala-sbt.org/).
Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details how to to contribute
to the project.

