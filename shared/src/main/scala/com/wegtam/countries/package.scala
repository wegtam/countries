/*
 * Copyright (c) 2020 Wegtam GmbH
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam

import eu.timepit.refined._
import eu.timepit.refined.api._
import eu.timepit.refined.auto._
import eu.timepit.refined.boolean._
import eu.timepit.refined.cats._
import eu.timepit.refined.collection._
import eu.timepit.refined.string._

package object countries {

  // A string containing a country code according to ISO-3166 alpha-2 which
  // must match the given regular expression.
  type Alpha2CountryCode = String Refined MatchesRegex[W.`"^[A-Z]{2}$"`.T]
  object Alpha2CountryCode
      extends RefinedTypeOps[Alpha2CountryCode, String]
      with CatsRefinedTypeOpsSyntax

  // A string containing a country code according to ISO-3166 alpha-3 which
  // must match the given regular expression.
  type Alpha3CountryCode = String Refined MatchesRegex[W.`"^[A-Z]{3}$"`.T]
  object Alpha3CountryCode
      extends RefinedTypeOps[Alpha3CountryCode, String]
      with CatsRefinedTypeOpsSyntax

  // The human readable name of a country.
  type CountryName = String Refined (Trimmed And NonEmpty)
  object CountryName extends RefinedTypeOps[CountryName, String] with CatsRefinedTypeOpsSyntax

  // A string containing a country code according to ISO-3166 numeric which
  // must match the given regular expression.
  type NumericCountryCode = String Refined MatchesRegex[W.`"^[0-9]{3}$"`.T]
  object NumericCountryCode
      extends RefinedTypeOps[NumericCountryCode, String]
      with CatsRefinedTypeOpsSyntax

  // The human readable name of a state.
  type StateName = String Refined (Trimmed And NonEmpty)
  object StateName extends RefinedTypeOps[StateName, String] with CatsRefinedTypeOpsSyntax

  // The top level domain for a country.
  type TopLevelDomain = String Refined MatchesRegex[W.`"^\\\\.\\\\w+"`.T]
  object TopLevelDomain extends RefinedTypeOps[TopLevelDomain, String] with CatsRefinedTypeOpsSyntax

}
