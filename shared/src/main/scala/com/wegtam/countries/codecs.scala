/*
 * Copyright (c) 2020 Wegtam GmbH
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.countries

import eu.timepit.refined.auto._
import io.circe._

object codecs {

  object circe {

    /** Codecs for circe to decode and encode countries by the ISO-3166 alpha-2 country code.
      */
    object alpha2 {

      implicit val decodeCountry: Decoder[Country] = Decoder.decodeString.emap { s =>
        Alpha2CountryCode.from(s).flatMap(Country.fromAlpha2)
      }

      implicit val encodeCountry: Encoder[Country] =
        Encoder.encodeString.contramap[Country](_.alpha2)

    }

    /** Codecs for circe to decode and encode countries by the ISO-3166 alpha-3 country code.
      */
    object alpha3 {

      implicit val decodeCountry: Decoder[Country] = Decoder.decodeString.emap { s =>
        Alpha3CountryCode.from(s).flatMap(Country.fromAlpha3)
      }

      implicit val encodeCountry: Encoder[Country] =
        Encoder.encodeString.contramap[Country](_.alpha3)

    }

    /** Codecs for circe to decode and encode countries by the ISO-3166 numeric country code.
      */
    object numeric {

      implicit val decodeCountry: Decoder[Country] = Decoder.decodeString.emap { s =>
        NumericCountryCode.from(s).flatMap(Country.fromNumeric)
      }

      implicit val encodeCountry: Encoder[Country] =
        Encoder.encodeString.contramap[Country](_.numeric)

    }
  }

}
