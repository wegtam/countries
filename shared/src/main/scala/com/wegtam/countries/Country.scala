/*
 * Copyright (c) 2020 Wegtam GmbH
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.countries

import cats.syntax.either._
import cats.syntax.eq._
import enumeratum._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._

import scala.collection.immutable._

/** Information about a country which holds data to make it identifiable according to ISO-3166.
  *
  * @param alpha2
  *   The ISO-3166 alpha-2 country code.
  * @param alpha3
  *   The ISO-3166 alpha-3 country code.
  * @param name
  *   The human readable country name in english language.
  * @param nameOfficial
  *   The human readable official state name in english language.
  * @param numeric
  *   The ISO-3166 numeric country code.
  * @param tld
  *   The top level domain(s) belonging to the country.
  */
sealed abstract class Country(
    val alpha2: Alpha2CountryCode,
    val alpha3: Alpha3CountryCode,
    val name: CountryName,
    val nameOfficial: StateName,
    val numeric: NumericCountryCode,
    val tld: List[TopLevelDomain]
) extends EnumEntry

object Country extends Enum[Country] with CatsEnum[Country] {
  implicit val orderingCountry: Ordering[Country] =
    (x: Country, y: Country) => x.name.compareTo(y.name)

  override val values: IndexedSeq[Country] = findValues

  /** Return the country for the given ISO-3166 alpha-2 country code.
    *
    * @param a
    *   An ISO-3166 alpha-2 country code.
    * @return
    *   The country if it exists or an error message.
    */
  def fromAlpha2(a: Alpha2CountryCode): Either[String, Country] =
    values.find(_.alpha2 === a).fold("No such country!".asLeft[Country])(_.asRight[String])

  /** Return the country for the given ISO-3166 alpha-3 country code.
    *
    * @param a
    *   An ISO-3166 alpha-3 country code.
    * @return
    *   The country if it exists or an error message.
    */
  def fromAlpha3(a: Alpha3CountryCode): Either[String, Country] =
    values.find(_.alpha3 === a).fold("No such country!".asLeft[Country])(_.asRight[String])

  /** Return the country for the given ISO-3166 numeric country code.
    *
    * @param n
    *   An ISO-3166 numeric country code.
    * @return
    *   The country if it exists or an error message.
    */
  def fromNumeric(n: NumericCountryCode): Either[String, Country] =
    values.find(_.numeric === n).fold("No such country!".asLeft[Country])(_.asRight[String])

  case object Afghanistan
      extends Country(
        alpha2 = "AF",
        alpha3 = "AFG",
        name = "Afghanistan",
        nameOfficial = "The Islamic Republic of Afghanistan",
        numeric = "004",
        tld = List(".af")
      )
  case object AlandIslands
      extends Country(
        alpha2 = "AX",
        alpha3 = "ALA",
        name = "Åland Islands",
        nameOfficial = "Åland",
        numeric = "248",
        tld = List(".ax")
      )
  case object Albania
      extends Country(
        alpha2 = "AL",
        alpha3 = "ALB",
        name = "Albania",
        nameOfficial = "The Republic of Albania",
        numeric = "008",
        tld = List(".al")
      )
  case object Algeria
      extends Country(
        alpha2 = "DZ",
        alpha3 = "DZA",
        name = "Algeria",
        nameOfficial = "The People's Democratic Republic of Algeria",
        numeric = "012",
        tld = List(".dz")
      )
  case object AmericanSamoa
      extends Country(
        alpha2 = "AS",
        alpha3 = "ASM",
        name = "American Samoa",
        nameOfficial = "The Territory of American Samoa",
        numeric = "016",
        tld = List(".as")
      )
  case object Andorra
      extends Country(
        alpha2 = "AD",
        alpha3 = "AND",
        name = "Andorra",
        nameOfficial = "The Principality of Andorra",
        numeric = "020",
        tld = List(".ad")
      )
  case object Angola
      extends Country(
        alpha2 = "AO",
        alpha3 = "AGO",
        name = "Angola",
        nameOfficial = "The Republic of Angola",
        numeric = "024",
        tld = List(".ao")
      )
  case object Anguilla
      extends Country(
        alpha2 = "AI",
        alpha3 = "AIA",
        name = "Anguilla",
        nameOfficial = "Anguilla",
        numeric = "660",
        tld = List(".ai")
      )
  case object Antarctica
      extends Country(
        alpha2 = "AQ",
        alpha3 = "ATA",
        name = "Antarctica",
        nameOfficial = "All land and ice shelves south of the 60th parallel south",
        numeric = "010",
        tld = List(".aq")
      )
  case object AntiguaBarbuda
      extends Country(
        alpha2 = "AG",
        alpha3 = "ATG",
        name = "Antigua and Barbuda",
        nameOfficial = "Antigua and Barbuda",
        numeric = "028",
        tld = List(".ag")
      )
  case object Argentina
      extends Country(
        alpha2 = "AR",
        alpha3 = "ARG",
        name = "Argentina",
        nameOfficial = "The Argentine Republic",
        numeric = "032",
        tld = List(".ar")
      )
  case object Armenia
      extends Country(
        alpha2 = "AM",
        alpha3 = "ARM",
        name = "Armenia",
        nameOfficial = "The Republic of Armenia",
        numeric = "051",
        tld = List(".am")
      )
  case object Aruba
      extends Country(
        alpha2 = "AW",
        alpha3 = "ABW",
        name = "Aruba",
        nameOfficial = "Aruba",
        numeric = "533",
        tld = List(".aw")
      )
  case object Australia
      extends Country(
        alpha2 = "AU",
        alpha3 = "AUS",
        name = "Australia",
        nameOfficial = "The Commonwealth of Australia",
        numeric = "036",
        tld = List(".au")
      )
  case object Austria
      extends Country(
        alpha2 = "AT",
        alpha3 = "AUT",
        name = "Austria",
        nameOfficial = "The Republic of Austria",
        numeric = "040",
        tld = List(".at")
      )
  case object Azerbaijan
      extends Country(
        alpha2 = "AZ",
        alpha3 = "AZE",
        name = "Azerbaijan",
        nameOfficial = "The Republic of Azerbaijan",
        numeric = "031",
        tld = List(".az")
      )
  case object Bahamas
      extends Country(
        alpha2 = "BS",
        alpha3 = "BHS",
        name = "Bahamas (the)",
        nameOfficial = "The Commonwealth of The Bahamas",
        numeric = "044",
        tld = List(".bs")
      )
  case object Bahrain
      extends Country(
        alpha2 = "BH",
        alpha3 = "BHR",
        name = "Bahrain",
        nameOfficial = "The Kingdom of Bahrain",
        numeric = "048",
        tld = List(".bh")
      )
  case object Bangladesh
      extends Country(
        alpha2 = "BD",
        alpha3 = "BGD",
        name = "Bangladesh",
        nameOfficial = "The People's Republic of Bangladesh",
        numeric = "050",
        tld = List(".bd")
      )
  case object Barbados
      extends Country(
        alpha2 = "BB",
        alpha3 = "BRB",
        name = "Barbados",
        nameOfficial = "Barbados",
        numeric = "052",
        tld = List(".bb")
      )
  case object Belarus
      extends Country(
        alpha2 = "BY",
        alpha3 = "BLR",
        name = "Belarus",
        nameOfficial = "The Republic of Belarus",
        numeric = "112",
        tld = List(".by")
      )
  case object Belgium
      extends Country(
        alpha2 = "BE",
        alpha3 = "BEL",
        name = "Belgium",
        nameOfficial = "The Kingdom of Belgium",
        numeric = "056",
        tld = List(".be")
      )
  case object Belize
      extends Country(
        alpha2 = "BZ",
        alpha3 = "BLZ",
        name = "Belize",
        nameOfficial = "Belize",
        numeric = "084",
        tld = List(".bz")
      )
  case object Benin
      extends Country(
        alpha2 = "BJ",
        alpha3 = "BEN",
        name = "Benin",
        nameOfficial = "The Republic of Benin",
        numeric = "204",
        tld = List(".bj")
      )
  case object Bermuda
      extends Country(
        alpha2 = "BM",
        alpha3 = "BMU",
        name = "Bermuda",
        nameOfficial = "Bermuda",
        numeric = "060",
        tld = List(".bm")
      )
  case object Bhutan
      extends Country(
        alpha2 = "BT",
        alpha3 = "BTN",
        name = "Bhutan",
        nameOfficial = "The Kingdom of Bhutan",
        numeric = "064",
        tld = List(".bt")
      )
  case object Bolivia
      extends Country(
        alpha2 = "BO",
        alpha3 = "BOL",
        name = "Bolivia (Plurinational State of)",
        nameOfficial = "The Plurinational State of Bolivia",
        numeric = "068",
        tld = List(".bo")
      )
  case object BonaireSintEustatiusSaba
      extends Country(
        alpha2 = "BQ",
        alpha3 = "BES",
        name = "Bonaire Sint Eustatius Saba",
        nameOfficial = "Bonaire, Sint Eustatius and Saba",
        numeric = "535",
        tld = List(".bq")
      )
  case object BosniaHerzegovina
      extends Country(
        alpha2 = "BA",
        alpha3 = "BIH",
        name = "Bosnia and Herzegovina",
        nameOfficial = "Bosnia and Herzegovina",
        numeric = "070",
        tld = List(".ba")
      )
  case object Botswana
      extends Country(
        alpha2 = "BW",
        alpha3 = "BWA",
        name = "Botswana",
        nameOfficial = "The Republic of Botswana",
        numeric = "072",
        tld = List(".bw")
      )
  case object BouvetIsland
      extends Country(
        alpha2 = "BV",
        alpha3 = "BVT",
        name = "Bouvet Island",
        nameOfficial = "Bouvet Island",
        numeric = "074",
        tld = List.empty
      )
  case object Brazil
      extends Country(
        alpha2 = "BR",
        alpha3 = "BRA",
        name = "Brazil",
        nameOfficial = "The Federative Republic of Brazil",
        numeric = "076",
        tld = List(".br")
      )
  case object BritishIndianOceanTerritory
      extends Country(
        alpha2 = "IO",
        alpha3 = "IOT",
        name = "British Indian Ocean Territory (the)",
        nameOfficial = "The British Indian Ocean Territory",
        numeric = "086",
        tld = List(".io")
      )
  case object BruneiDarussalam
      extends Country(
        alpha2 = "BN",
        alpha3 = "BRN",
        name = "Brunei Darussalam",
        nameOfficial = "The Nation of Brunei, the Abode of Peace",
        numeric = "096",
        tld = List(".bn")
      )
  case object Bulgaria
      extends Country(
        alpha2 = "BG",
        alpha3 = "BGR",
        name = "Bulgaria",
        nameOfficial = "The Republic of Bulgaria",
        numeric = "100",
        tld = List(".bg")
      )
  case object BurkinaFaso
      extends Country(
        alpha2 = "BF",
        alpha3 = "BFA",
        name = "Burkina Faso",
        nameOfficial = "Burkina Faso",
        numeric = "854",
        tld = List(".bf")
      )
  case object Burundi
      extends Country(
        alpha2 = "BI",
        alpha3 = "BDI",
        name = "Burundi",
        nameOfficial = "The Republic of Burundi",
        numeric = "108",
        tld = List(".bi")
      )
  case object CaboVerde
      extends Country(
        alpha2 = "CV",
        alpha3 = "CPV",
        name = "Cabo Verde",
        nameOfficial = "The Republic of Cabo Verde",
        numeric = "132",
        tld = List(".cv")
      )
  case object Cambodia
      extends Country(
        alpha2 = "KH",
        alpha3 = "KHM",
        name = "Cambodia",
        nameOfficial = "The Kingdom of Cambodia",
        numeric = "116",
        tld = List(".kh")
      )
  case object Cameroon
      extends Country(
        alpha2 = "CM",
        alpha3 = "CMR",
        name = "Cameroon",
        nameOfficial = "The Republic of Cameroon",
        numeric = "120",
        tld = List(".cm")
      )
  case object Canada
      extends Country(
        alpha2 = "CA",
        alpha3 = "CAN",
        name = "Canada",
        nameOfficial = "Canada",
        numeric = "124",
        tld = List(".ca")
      )
  case object CaymanIslands
      extends Country(
        alpha2 = "KY",
        alpha3 = "CYM",
        name = "Cayman Islands (the)",
        nameOfficial = "The Cayman Islands",
        numeric = "136",
        tld = List(".ky")
      )
  case object CentralAfricanRepublic
      extends Country(
        alpha2 = "CF",
        alpha3 = "CAF",
        name = "Central African Republic (the)",
        nameOfficial = "The Central African Republic",
        numeric = "140",
        tld = List(".cf")
      )
  case object Chad
      extends Country(
        alpha2 = "TD",
        alpha3 = "TCD",
        name = "Chad",
        nameOfficial = "The Republic of Chad",
        numeric = "148",
        tld = List(".td")
      )
  case object Chile
      extends Country(
        alpha2 = "CL",
        alpha3 = "CHL",
        name = "Chile",
        nameOfficial = "The Republic of Chile",
        numeric = "152",
        tld = List(".cl")
      )
  case object China
      extends Country(
        alpha2 = "CN",
        alpha3 = "CHN",
        name = "China",
        nameOfficial = "The People's Republic of China",
        numeric = "156",
        tld = List(".cn")
      )
  case object ChristmasIsland
      extends Country(
        alpha2 = "CX",
        alpha3 = "CXR",
        name = "Christmas Island",
        nameOfficial = "The Territory of Christmas Island",
        numeric = "162",
        tld = List(".cx")
      )
  case object CocosKeelingIslands
      extends Country(
        alpha2 = "CC",
        alpha3 = "CCK",
        name = "Cocos (Keeling) Islands (the)",
        nameOfficial = "The Territory of Cocos (Keeling) Islands",
        numeric = "166",
        tld = List(".cc")
      )
  case object Colombia
      extends Country(
        alpha2 = "CO",
        alpha3 = "COL",
        name = "Colombia",
        nameOfficial = "The Republic of Colombia",
        numeric = "170",
        tld = List(".co")
      )
  case object Comoros
      extends Country(
        alpha2 = "KM",
        alpha3 = "COM",
        name = "Comoros (the)",
        nameOfficial = "The Union of the Comoros",
        numeric = "174",
        tld = List(".km")
      )
  case object CongoDemocraticRepublic
      extends Country(
        alpha2 = "CD",
        alpha3 = "COD",
        name = "Congo (the Democratic Republic of the)",
        nameOfficial = "The Democratic Republic of the Congo",
        numeric = "180",
        tld = List(".cd")
      )
  case object Congo
      extends Country(
        alpha2 = "CG",
        alpha3 = "COG",
        name = "Congo (the)",
        nameOfficial = "The Republic of the Congo",
        numeric = "178",
        tld = List(".cg")
      )
  case object CookIslands
      extends Country(
        alpha2 = "CK",
        alpha3 = "COK",
        name = "Cook Islands (the)",
        nameOfficial = "The Cook Islands",
        numeric = "184",
        tld = List(".ck")
      )
  case object CostaRica
      extends Country(
        alpha2 = "CR",
        alpha3 = "CRI",
        name = "Costa Rica",
        nameOfficial = "The Republic of Costa Rica",
        numeric = "188",
        tld = List(".cr")
      )
  case object CoteIvoire
      extends Country(
        alpha2 = "CI",
        alpha3 = "CIV",
        name = "Côte d'Ivoire",
        nameOfficial = "The Republic of Côte d'Ivoire",
        numeric = "384",
        tld = List(".ci")
      )
  case object Croatia
      extends Country(
        alpha2 = "HR",
        alpha3 = "HRV",
        name = "Croatia",
        nameOfficial = "The Republic of Croatia",
        numeric = "191",
        tld = List(".hr")
      )
  case object Cuba
      extends Country(
        alpha2 = "CU",
        alpha3 = "CUB",
        name = "Cuba",
        nameOfficial = "The Republic of Cuba",
        numeric = "192",
        tld = List(".cu")
      )
  case object Curacao
      extends Country(
        alpha2 = "CW",
        alpha3 = "CUW",
        name = "Curaçao",
        nameOfficial = "The Country of Curaçao",
        numeric = "531",
        tld = List(".cw")
      )
  case object Cyprus
      extends Country(
        alpha2 = "CY",
        alpha3 = "CYP",
        name = "Cyprus",
        nameOfficial = "The Republic of Cyprus",
        numeric = "196",
        tld = List(".cy")
      )
  case object Czechia
      extends Country(
        alpha2 = "CZ",
        alpha3 = "CZE",
        name = "Czechia",
        nameOfficial = "The Czech Republic",
        numeric = "203",
        tld = List(".cz")
      )
  case object Denmark
      extends Country(
        alpha2 = "DK",
        alpha3 = "DNK",
        name = "Denmark",
        nameOfficial = "The Kingdom of Denmark",
        numeric = "208",
        tld = List(".dk")
      )
  case object Djibouti
      extends Country(
        alpha2 = "DJ",
        alpha3 = "DJI",
        name = "Djibouti",
        nameOfficial = "The Republic of Djibouti",
        numeric = "262",
        tld = List(".dj")
      )
  case object Dominica
      extends Country(
        alpha2 = "DM",
        alpha3 = "DMA",
        name = "Dominica",
        nameOfficial = "The Commonwealth of Dominica",
        numeric = "212",
        tld = List(".dm")
      )
  case object DominicanRepublic
      extends Country(
        alpha2 = "DO",
        alpha3 = "DOM",
        name = "Dominican Republic (the)",
        nameOfficial = "The Dominican Republic",
        numeric = "214",
        tld = List(".do")
      )
  case object Ecuador
      extends Country(
        alpha2 = "EC",
        alpha3 = "ECU",
        name = "Ecuador",
        nameOfficial = "The Republic of Ecuador",
        numeric = "218",
        tld = List(".ec")
      )
  case object Egypt
      extends Country(
        alpha2 = "EG",
        alpha3 = "EGY",
        name = "Egypt",
        nameOfficial = "The Arab Republic of Egypt",
        numeric = "818",
        tld = List(".eg")
      )
  case object ElSalvador
      extends Country(
        alpha2 = "SV",
        alpha3 = "SLV",
        name = "El Salvador",
        nameOfficial = "The Republic of El Salvador",
        numeric = "222",
        tld = List(".sv")
      )
  case object EquatorialGuinea
      extends Country(
        alpha2 = "GQ",
        alpha3 = "GNQ",
        name = "Equatorial Guinea",
        nameOfficial = "The Republic of Equatorial Guinea",
        numeric = "226",
        tld = List(".gq")
      )
  case object Eritrea
      extends Country(
        alpha2 = "ER",
        alpha3 = "ERI",
        name = "Eritrea",
        nameOfficial = "The State of Eritrea",
        numeric = "232",
        tld = List(".er")
      )
  case object Estonia
      extends Country(
        alpha2 = "EE",
        alpha3 = "EST",
        name = "Estonia",
        nameOfficial = "The Republic of Estonia",
        numeric = "233",
        tld = List(".ee")
      )
  case object Eswatini
      extends Country(
        alpha2 = "SZ",
        alpha3 = "SWZ",
        name = "Eswatini",
        nameOfficial = "The Kingdom of Eswatini",
        numeric = "748",
        tld = List(".sz")
      )
  case object Ethiopia
      extends Country(
        alpha2 = "ET",
        alpha3 = "ETH",
        name = "Ethiopia",
        nameOfficial = "The Federal Democratic Republic of Ethiopia",
        numeric = "231",
        tld = List(".et")
      )
  case object FalklandIslands
      extends Country(
        alpha2 = "FK",
        alpha3 = "FLK",
        name = "Falkland Islands (the) [Malvinas]",
        nameOfficial = "The Falkland Islands",
        numeric = "238",
        tld = List(".fk")
      )
  case object FaroeIslands
      extends Country(
        alpha2 = "FO",
        alpha3 = "FRO",
        name = "Faroe Islands (the)",
        nameOfficial = "The Faroe Islands",
        numeric = "234",
        tld = List(".fo")
      )
  case object Fiji
      extends Country(
        alpha2 = "FJ",
        alpha3 = "FJI",
        name = "Fiji",
        nameOfficial = "The Republic of Fiji",
        numeric = "242",
        tld = List(".fj")
      )
  case object Finland
      extends Country(
        alpha2 = "FI",
        alpha3 = "FIN",
        name = "Finland",
        nameOfficial = "The Republic of Finland",
        numeric = "246",
        tld = List(".fi")
      )
  case object France
      extends Country(
        alpha2 = "FR",
        alpha3 = "FRA",
        name = "France",
        nameOfficial = "The French Republic",
        numeric = "250",
        tld = List(".fr")
      )
  case object FrenchGuiana
      extends Country(
        alpha2 = "GF",
        alpha3 = "GUF",
        name = "French Guiana",
        nameOfficial = "Guyane",
        numeric = "254",
        tld = List(".gf")
      )
  case object FrenchPolynesia
      extends Country(
        alpha2 = "PF",
        alpha3 = "PYF",
        name = "French Polynesia",
        nameOfficial = "French Polynesia",
        numeric = "258",
        tld = List(".pf")
      )
  case object FrenchSouthernTerritories
      extends Country(
        alpha2 = "TF",
        alpha3 = "ATF",
        name = "French Southern Territories (the)",
        nameOfficial = "The French Southern and Antarctic Lands",
        numeric = "260",
        tld = List(".tf")
      )
  case object Gabon
      extends Country(
        alpha2 = "GA",
        alpha3 = "GAB",
        name = "Gabon",
        nameOfficial = "The Gabonese Republic",
        numeric = "266",
        tld = List(".ga")
      )
  case object Gambia
      extends Country(
        alpha2 = "GM",
        alpha3 = "GMB",
        name = "Gambia (the)",
        nameOfficial = "The Republic of The Gambia",
        numeric = "270",
        tld = List(".gm")
      )
  case object Georgia
      extends Country(
        alpha2 = "GE",
        alpha3 = "GEO",
        name = "Georgia",
        nameOfficial = "Georgia",
        numeric = "268",
        tld = List(".ge")
      )
  case object Germany
      extends Country(
        alpha2 = "DE",
        alpha3 = "DEU",
        name = "Germany",
        nameOfficial = "The Federal Republic of Germany",
        numeric = "276",
        tld = List(".de")
      )
  case object Ghana
      extends Country(
        alpha2 = "GH",
        alpha3 = "GHA",
        name = "Ghana",
        nameOfficial = "The Republic of Ghana",
        numeric = "288",
        tld = List(".gh")
      )
  case object Gibraltar
      extends Country(
        alpha2 = "GI",
        alpha3 = "GIB",
        name = "Gibraltar",
        nameOfficial = "Gibraltar",
        numeric = "292",
        tld = List(".gi")
      )
  case object Greece
      extends Country(
        alpha2 = "GR",
        alpha3 = "GRC",
        name = "Greece",
        nameOfficial = "The Hellenic Republic",
        numeric = "300",
        tld = List(".gr")
      )
  case object Greenland
      extends Country(
        alpha2 = "GL",
        alpha3 = "GRL",
        name = "Greenland",
        nameOfficial = "Kalaallit Nunaat",
        numeric = "304",
        tld = List(".gl")
      )
  case object Grenada
      extends Country(
        alpha2 = "GD",
        alpha3 = "GRD",
        name = "Grenada",
        nameOfficial = "Grenada",
        numeric = "308",
        tld = List(".gd")
      )
  case object Guadeloupe
      extends Country(
        alpha2 = "GP",
        alpha3 = "GLP",
        name = "Guadeloupe",
        nameOfficial = "Guadeloupe",
        numeric = "312",
        tld = List(".gp")
      )
  case object Guam
      extends Country(
        alpha2 = "GU",
        alpha3 = "GUM",
        name = "Guam",
        nameOfficial = "The Territory of Guam",
        numeric = "316",
        tld = List(".gu")
      )
  case object Guatemala
      extends Country(
        alpha2 = "GT",
        alpha3 = "GTM",
        name = "Guatemala",
        nameOfficial = "The Republic of Guatemala",
        numeric = "320",
        tld = List(".gt")
      )
  case object Guernsey
      extends Country(
        alpha2 = "GG",
        alpha3 = "GGY",
        name = "Guernsey",
        nameOfficial = "The Bailiwick of Guernsey",
        numeric = "831",
        tld = List(".gg")
      )
  case object Guinea
      extends Country(
        alpha2 = "GN",
        alpha3 = "GIN",
        name = "Guinea",
        nameOfficial = "The Republic of Guinea",
        numeric = "324",
        tld = List(".gn")
      )
  case object GuineaBissau
      extends Country(
        alpha2 = "GW",
        alpha3 = "GNB",
        name = "Guinea-Bissau",
        nameOfficial = "The Republic of Guinea-Bissau",
        numeric = "624",
        tld = List(".gw")
      )
  case object Guyana
      extends Country(
        alpha2 = "GY",
        alpha3 = "GUY",
        name = "Guyana",
        nameOfficial = "The Co-operative Republic of Guyana",
        numeric = "328",
        tld = List(".gy")
      )
  case object Haiti
      extends Country(
        alpha2 = "HT",
        alpha3 = "HTI",
        name = "Haiti",
        nameOfficial = "The Republic of Haiti",
        numeric = "332",
        tld = List(".ht")
      )
  case object HeardIslandAndMcDonaldIslands
      extends Country(
        alpha2 = "HM",
        alpha3 = "HMD",
        name = "Heard Island and McDonald Islands",
        nameOfficial = "The Territory of Heard Island and McDonald Islands",
        numeric = "334",
        tld = List(".hm")
      )
  case object HolySee
      extends Country(
        alpha2 = "VA",
        alpha3 = "VAT",
        name = "Holy See (the)",
        nameOfficial = "The Holy See",
        numeric = "336",
        tld = List(".va")
      )
  case object Honduras
      extends Country(
        alpha2 = "HN",
        alpha3 = "HND",
        name = "Honduras",
        nameOfficial = "The Republic of Honduras",
        numeric = "340",
        tld = List(".hn")
      )
  case object HongKong
      extends Country(
        alpha2 = "HK",
        alpha3 = "HKG",
        name = "Hong Kong",
        nameOfficial = "The Hong Kong Special Administrative Region of China",
        numeric = "344",
        tld = List(".hk")
      )
  case object Hungary
      extends Country(
        alpha2 = "HU",
        alpha3 = "HUN",
        name = "Hungary",
        nameOfficial = "Hungary",
        numeric = "348",
        tld = List(".hu")
      )
  case object Iceland
      extends Country(
        alpha2 = "IS",
        alpha3 = "ISL",
        name = "Iceland",
        nameOfficial = "Iceland",
        numeric = "352",
        tld = List(".is")
      )
  case object India
      extends Country(
        alpha2 = "IN",
        alpha3 = "IND",
        name = "India",
        nameOfficial = "The Republic of India",
        numeric = "356",
        tld = List(".in")
      )
  case object Indonesia
      extends Country(
        alpha2 = "ID",
        alpha3 = "IDN",
        name = "Indonesia",
        nameOfficial = "The Republic of Indonesia",
        numeric = "360",
        tld = List(".id")
      )
  case object Iran
      extends Country(
        alpha2 = "IR",
        alpha3 = "IRN",
        name = "Iran (Islamic Republic of)",
        nameOfficial = "The Islamic Republic of Iran",
        numeric = "364",
        tld = List(".ir")
      )
  case object Iraq
      extends Country(
        alpha2 = "IQ",
        alpha3 = "IRQ",
        name = "Iraq",
        nameOfficial = "The Republic of Iraq",
        numeric = "368",
        tld = List(".iq")
      )
  case object Ireland
      extends Country(
        alpha2 = "IE",
        alpha3 = "IRL",
        name = "Ireland",
        nameOfficial = "Ireland",
        numeric = "372",
        tld = List(".ie")
      )
  case object IsleOfMan
      extends Country(
        alpha2 = "IM",
        alpha3 = "IMN",
        name = "Isle of Man",
        nameOfficial = "The Isle of Man",
        numeric = "833",
        tld = List(".im")
      )
  case object Israel
      extends Country(
        alpha2 = "IL",
        alpha3 = "ISR",
        name = "Israel",
        nameOfficial = "The State of Israel",
        numeric = "376",
        tld = List(".il")
      )
  case object Italy
      extends Country(
        alpha2 = "IT",
        alpha3 = "ITA",
        name = "Italy",
        nameOfficial = "The Italian Republic",
        numeric = "380",
        tld = List(".it")
      )
  case object Jamaica
      extends Country(
        alpha2 = "JM",
        alpha3 = "JAM",
        name = "Jamaica",
        nameOfficial = "Jamaica",
        numeric = "388",
        tld = List(".jm")
      )
  case object Japan
      extends Country(
        alpha2 = "JP",
        alpha3 = "JPN",
        name = "Japan",
        nameOfficial = "Japan",
        numeric = "392",
        tld = List(".jp")
      )
  case object Jersey
      extends Country(
        alpha2 = "JE",
        alpha3 = "JEY",
        name = "Jersey",
        nameOfficial = "The Bailiwick of Jersey",
        numeric = "832",
        tld = List(".je")
      )
  case object Jordan
      extends Country(
        alpha2 = "JO",
        alpha3 = "JOR",
        name = "Jordan",
        nameOfficial = "The Hashemite Kingdom of Jordan",
        numeric = "400",
        tld = List(".jo")
      )
  case object Kazakhstan
      extends Country(
        alpha2 = "KZ",
        alpha3 = "KAZ",
        name = "Kazakhstan",
        nameOfficial = "The Republic of Kazakhstan",
        numeric = "398",
        tld = List(".kz")
      )
  case object Kenya
      extends Country(
        alpha2 = "KE",
        alpha3 = "KEN",
        name = "Kenya",
        nameOfficial = "The Republic of Kenya",
        numeric = "404",
        tld = List(".ke")
      )
  case object Kiribati
      extends Country(
        alpha2 = "KI",
        alpha3 = "KIR",
        name = "Kiribati",
        nameOfficial = "The Republic of Kiribati",
        numeric = "296",
        tld = List(".ki")
      )
  case object KoreaDemocraticPeoplesRepublic
      extends Country(
        alpha2 = "KP",
        alpha3 = "PRK",
        name = "Korea (the Democratic People's Republic of)",
        nameOfficial = "The Democratic People's Republic of Korea",
        numeric = "408",
        tld = List(".kp")
      )
  case object KoreaRepublic
      extends Country(
        alpha2 = "KR",
        alpha3 = "KOR",
        name = "Korea (the Republic of)",
        nameOfficial = "The Republic of Korea",
        numeric = "410",
        tld = List(".kr")
      )
  case object Kuwait
      extends Country(
        alpha2 = "KW",
        alpha3 = "KWT",
        name = "Kuwait",
        nameOfficial = "The State of Kuwait",
        numeric = "414",
        tld = List(".kw")
      )
  case object Kyrgyzstan
      extends Country(
        alpha2 = "KG",
        alpha3 = "KGZ",
        name = "Kyrgyzstan",
        nameOfficial = "The Kyrgyz Republic",
        numeric = "417",
        tld = List(".kg")
      )
  case object LaoPeoplesDemocraticRepublic
      extends Country(
        alpha2 = "LA",
        alpha3 = "LAO",
        name = "Lao People's Democratic Republic (the)",
        nameOfficial = "The Lao People's Democratic Republic",
        numeric = "418",
        tld = List(".la")
      )
  case object Latvia
      extends Country(
        alpha2 = "LV",
        alpha3 = "LVA",
        name = "Latvia",
        nameOfficial = "The Republic of Latvia",
        numeric = "428",
        tld = List(".lv")
      )
  case object Lebanon
      extends Country(
        alpha2 = "LB",
        alpha3 = "LBN",
        name = "Lebanon",
        nameOfficial = "The Lebanese Republic",
        numeric = "422",
        tld = List(".lb")
      )
  case object Lesotho
      extends Country(
        alpha2 = "LS",
        alpha3 = "LSO",
        name = "Lesotho",
        nameOfficial = "The Kingdom of Lesotho",
        numeric = "426",
        tld = List(".ls")
      )
  case object Liberia
      extends Country(
        alpha2 = "LR",
        alpha3 = "LBR",
        name = "Liberia",
        nameOfficial = "The Republic of Liberia",
        numeric = "430",
        tld = List(".lr")
      )
  case object Libya
      extends Country(
        alpha2 = "LY",
        alpha3 = "LBY",
        name = "Libya",
        nameOfficial = "The State of Libya",
        numeric = "434",
        tld = List(".ly")
      )
  case object Liechtenstein
      extends Country(
        alpha2 = "LI",
        alpha3 = "LIE",
        name = "Liechtenstein",
        nameOfficial = "The Principality of Liechtenstein",
        numeric = "438",
        tld = List(".li")
      )
  case object Lithuania
      extends Country(
        alpha2 = "LT",
        alpha3 = "LTU",
        name = "Lithuania",
        nameOfficial = "The Republic of Lithuania",
        numeric = "440",
        tld = List(".lt")
      )
  case object Luxembourg
      extends Country(
        alpha2 = "LU",
        alpha3 = "LUX",
        name = "Luxembourg",
        nameOfficial = "The Grand Duchy of Luxembourg",
        numeric = "442",
        tld = List(".lu")
      )
  case object Macao
      extends Country(
        alpha2 = "MO",
        alpha3 = "MAC",
        name = "Macao",
        nameOfficial = "Macao Special Administrative Region of China",
        numeric = "446",
        tld = List(".mo")
      )
  case object NorthMacedonia
      extends Country(
        alpha2 = "MK",
        alpha3 = "MKD",
        name = "North Macedonia",
        nameOfficial = "Republic of North Macedonia",
        numeric = "807",
        tld = List(".mk")
      )
  case object Madagascar
      extends Country(
        alpha2 = "MG",
        alpha3 = "MDG",
        name = "Madagascar",
        nameOfficial = "The Republic of Madagascar",
        numeric = "450",
        tld = List(".mg")
      )
  case object Malawi
      extends Country(
        alpha2 = "MW",
        alpha3 = "MWI",
        name = "Malawi",
        nameOfficial = "The Republic of Malawi",
        numeric = "454",
        tld = List(".mw")
      )
  case object Malaysia
      extends Country(
        alpha2 = "MY",
        alpha3 = "MYS",
        name = "Malaysia",
        nameOfficial = "Malaysia",
        numeric = "458",
        tld = List(".my")
      )
  case object Maldives
      extends Country(
        alpha2 = "MV",
        alpha3 = "MDV",
        name = "Maldives",
        nameOfficial = "The Republic of Maldives",
        numeric = "462",
        tld = List(".mv")
      )
  case object Mali
      extends Country(
        alpha2 = "ML",
        alpha3 = "MLI",
        name = "Mali",
        nameOfficial = "The Republic of Mali",
        numeric = "466",
        tld = List(".ml")
      )
  case object Malta
      extends Country(
        alpha2 = "MT",
        alpha3 = "MLT",
        name = "Malta",
        nameOfficial = "The Republic of Malta",
        numeric = "470",
        tld = List(".mt")
      )
  case object MarshallIslands
      extends Country(
        alpha2 = "MH",
        alpha3 = "MHL",
        name = "Marshall Islands (the)",
        nameOfficial = "The Republic of the Marshall Islands",
        numeric = "584",
        tld = List(".mh")
      )
  case object Martinique
      extends Country(
        alpha2 = "MQ",
        alpha3 = "MTQ",
        name = "Martinique",
        nameOfficial = "Martinique",
        numeric = "474",
        tld = List(".mq")
      )
  case object Mauritania
      extends Country(
        alpha2 = "MR",
        alpha3 = "MRT",
        name = "Mauritania",
        nameOfficial = "The Islamic Republic of Mauritania",
        numeric = "478",
        tld = List(".mr")
      )
  case object Mauritius
      extends Country(
        alpha2 = "MU",
        alpha3 = "MUS",
        name = "Mauritius",
        nameOfficial = "The Republic of Mauritius",
        numeric = "480",
        tld = List(".mu")
      )
  case object Mayotte
      extends Country(
        alpha2 = "YT",
        alpha3 = "MYT",
        name = "Mayotte",
        nameOfficial = "The Department of Mayotte",
        numeric = "175",
        tld = List(".yt")
      )
  case object Mexico
      extends Country(
        alpha2 = "MX",
        alpha3 = "MEX",
        name = "Mexico",
        nameOfficial = "The United Mexican States",
        numeric = "484",
        tld = List(".mx")
      )
  case object Micronesia
      extends Country(
        alpha2 = "FM",
        alpha3 = "FSM",
        name = "Micronesia (Federated States of)",
        nameOfficial = "The Federated States of Micronesia",
        numeric = "583",
        tld = List(".fm")
      )
  case object Moldova
      extends Country(
        alpha2 = "MD",
        alpha3 = "MDA",
        name = "Moldova (the Republic of)",
        nameOfficial = "The Republic of Moldova",
        numeric = "498",
        tld = List(".md")
      )
  case object Monaco
      extends Country(
        alpha2 = "MC",
        alpha3 = "MCO",
        name = "Monaco",
        nameOfficial = "The Principality of Monaco",
        numeric = "492",
        tld = List(".mc")
      )
  case object Mongolia
      extends Country(
        alpha2 = "MN",
        alpha3 = "MNG",
        name = "Mongolia",
        nameOfficial = "The State of Mongolia",
        numeric = "496",
        tld = List(".mn")
      )
  case object Montenegro
      extends Country(
        alpha2 = "ME",
        alpha3 = "MNE",
        name = "Montenegro",
        nameOfficial = "Montenegro",
        numeric = "499",
        tld = List(".me")
      )
  case object Montserrat
      extends Country(
        alpha2 = "MS",
        alpha3 = "MSR",
        name = "Montserrat",
        nameOfficial = "Montserrat",
        numeric = "500",
        tld = List(".ms")
      )
  case object Morocco
      extends Country(
        alpha2 = "MA",
        alpha3 = "MAR",
        name = "Morocco",
        nameOfficial = "The Kingdom of Morocco",
        numeric = "504",
        tld = List(".ma")
      )
  case object Mozambique
      extends Country(
        alpha2 = "MZ",
        alpha3 = "MOZ",
        name = "Mozambique",
        nameOfficial = "The Republic of Mozambique",
        numeric = "508",
        tld = List(".mz")
      )
  case object Myanmar
      extends Country(
        alpha2 = "MM",
        alpha3 = "MMR",
        name = "Myanmar",
        nameOfficial = "The Republic of the Union of Myanmar",
        numeric = "104",
        tld = List(".mm")
      )
  case object Namibia
      extends Country(
        alpha2 = "NA",
        alpha3 = "NAM",
        name = "Namibia",
        nameOfficial = "The Republic of Namibia",
        numeric = "516",
        tld = List(".na")
      )
  case object Nauru
      extends Country(
        alpha2 = "NR",
        alpha3 = "NRU",
        name = "Nauru",
        nameOfficial = "The Republic of Nauru",
        numeric = "520",
        tld = List(".nr")
      )
  case object Nepal
      extends Country(
        alpha2 = "NP",
        alpha3 = "NPL",
        name = "Nepal",
        nameOfficial = "The Federal Democratic Republic of Nepal",
        numeric = "524",
        tld = List(".np")
      )
  case object Netherlands
      extends Country(
        alpha2 = "NL",
        alpha3 = "NLD",
        name = "Netherlands (the)",
        nameOfficial = "The Kingdom of the Netherlands",
        numeric = "528",
        tld = List(".nl")
      )
  case object NewCaledonia
      extends Country(
        alpha2 = "NC",
        alpha3 = "NCL",
        name = "New Caledonia",
        nameOfficial = "New Caledonia",
        numeric = "540",
        tld = List(".nc")
      )
  case object NewZealand
      extends Country(
        alpha2 = "NZ",
        alpha3 = "NZL",
        name = "New Zealand",
        nameOfficial = "New Zealand",
        numeric = "554",
        tld = List(".nz")
      )
  case object Nicaragua
      extends Country(
        alpha2 = "NI",
        alpha3 = "NIC",
        name = "Nicaragua",
        nameOfficial = "The Republic of Nicaragua",
        numeric = "558",
        tld = List(".ni")
      )
  case object Niger
      extends Country(
        alpha2 = "NE",
        alpha3 = "NER",
        name = "Niger (the)",
        nameOfficial = "The Republic of the Niger",
        numeric = "562",
        tld = List(".ne")
      )
  case object Nigeria
      extends Country(
        alpha2 = "NG",
        alpha3 = "NGA",
        name = "Nigeria",
        nameOfficial = "The Federal Republic of Nigeria",
        numeric = "566",
        tld = List(".ng")
      )
  case object Niue
      extends Country(
        alpha2 = "NU",
        alpha3 = "NIU",
        name = "Niue",
        nameOfficial = "Niue",
        numeric = "570",
        tld = List(".nu")
      )
  case object NorfolkIsland
      extends Country(
        alpha2 = "NF",
        alpha3 = "NFK",
        name = "Norfolk Island",
        nameOfficial = "The Territory of Norfolk Island",
        numeric = "574",
        tld = List(".nf")
      )
  case object NorthernMarianaIslands
      extends Country(
        alpha2 = "MP",
        alpha3 = "MNP",
        name = "Northern Mariana Islands (the)",
        nameOfficial = "The Commonwealth of the Northern Mariana Islands",
        numeric = "580",
        tld = List(".mp")
      )
  case object Norway
      extends Country(
        alpha2 = "NO",
        alpha3 = "NOR",
        name = "Norway",
        nameOfficial = "The Kingdom of Norway",
        numeric = "578",
        tld = List(".no")
      )
  case object Oman
      extends Country(
        alpha2 = "OM",
        alpha3 = "OMN",
        name = "Oman",
        nameOfficial = "The Sultanate of Oman",
        numeric = "512",
        tld = List(".om")
      )
  case object Pakistan
      extends Country(
        alpha2 = "PK",
        alpha3 = "PAK",
        name = "Pakistan",
        nameOfficial = "The Islamic Republic of Pakistan",
        numeric = "586",
        tld = List(".pk")
      )
  case object Palau
      extends Country(
        alpha2 = "PW",
        alpha3 = "PLW",
        name = "Palau",
        nameOfficial = "The Republic of Palau",
        numeric = "585",
        tld = List(".pw")
      )
  case object Palestine
      extends Country(
        alpha2 = "PS",
        alpha3 = "PSE",
        name = "Palestine, State of",
        nameOfficial = "The State of Palestine",
        numeric = "275",
        tld = List(".ps")
      )
  case object Panama
      extends Country(
        alpha2 = "PA",
        alpha3 = "PAN",
        name = "Panama",
        nameOfficial = "The Republic of Panamá",
        numeric = "591",
        tld = List(".pa")
      )
  case object PapuaNewGuinea
      extends Country(
        alpha2 = "PG",
        alpha3 = "PNG",
        name = "Papua New Guinea",
        nameOfficial = "The Independent State of Papua New Guinea",
        numeric = "598",
        tld = List(".pg")
      )
  case object Paraguay
      extends Country(
        alpha2 = "PY",
        alpha3 = "PRY",
        name = "Paraguay",
        nameOfficial = "The Republic of Paraguay",
        numeric = "600",
        tld = List(".py")
      )
  case object Peru
      extends Country(
        alpha2 = "PE",
        alpha3 = "PER",
        name = "Peru",
        nameOfficial = "The Republic of Perú",
        numeric = "604",
        tld = List(".pe")
      )
  case object Philippines
      extends Country(
        alpha2 = "PH",
        alpha3 = "PHL",
        name = "Philippines (the)",
        nameOfficial = "The Republic of the Philippines",
        numeric = "608",
        tld = List(".ph")
      )
  case object Pitcairn
      extends Country(
        alpha2 = "PN",
        alpha3 = "PCN",
        name = "Pitcairn",
        nameOfficial = "The Pitcairn, Henderson, Ducie and Oeno Islands",
        numeric = "612",
        tld = List(".pn")
      )
  case object Poland
      extends Country(
        alpha2 = "PL",
        alpha3 = "POL",
        name = "Poland",
        nameOfficial = "The Republic of Poland",
        numeric = "616",
        tld = List(".pl")
      )
  case object Portugal
      extends Country(
        alpha2 = "PT",
        alpha3 = "PRT",
        name = "Portugal",
        nameOfficial = "The Portuguese Republic",
        numeric = "620",
        tld = List(".pt")
      )
  case object PuertoRico
      extends Country(
        alpha2 = "PR",
        alpha3 = "PRI",
        name = "Puerto Rico",
        nameOfficial = "The Commonwealth of Puerto Rico",
        numeric = "630",
        tld = List(".pr")
      )
  case object Qatar
      extends Country(
        alpha2 = "QA",
        alpha3 = "QAT",
        name = "Qatar",
        nameOfficial = "The State of Qatar",
        numeric = "634",
        tld = List(".qa")
      )
  case object Reunion
      extends Country(
        alpha2 = "RE",
        alpha3 = "REU",
        name = "Réunion",
        nameOfficial = "Réunion",
        numeric = "638",
        tld = List(".re")
      )
  case object Romania
      extends Country(
        alpha2 = "RO",
        alpha3 = "ROU",
        name = "Romania",
        nameOfficial = "Romania",
        numeric = "642",
        tld = List(".ro")
      )
  case object RussianFederation
      extends Country(
        alpha2 = "RU",
        alpha3 = "RUS",
        name = "Russian Federation (the)",
        nameOfficial = "The Russian Federation",
        numeric = "643",
        tld = List(".ru")
      )
  case object Rwanda
      extends Country(
        alpha2 = "RW",
        alpha3 = "RWA",
        name = "Rwanda",
        nameOfficial = "The Republic of Rwanda",
        numeric = "646",
        tld = List(".rw")
      )
  case object SaintBarthelemy
      extends Country(
        alpha2 = "BL",
        alpha3 = "BLM",
        name = "Saint Barthélemy",
        nameOfficial = "The Collectivity of Saint-Barthélemy",
        numeric = "652",
        tld = List(".bl")
      )
  case object SaintHelenaAscensionIslandTristanDaCunha
      extends Country(
        alpha2 = "SH",
        alpha3 = "SHN",
        name = "Saint Helena Ascension Island Tristan da Cunha",
        nameOfficial = "Saint Helena, Ascension and Tristan da Cunha",
        numeric = "654",
        tld = List(".sh")
      )
  case object SaintKittsAndNevis
      extends Country(
        alpha2 = "KN",
        alpha3 = "KNA",
        name = "Saint Kitts and Nevis",
        nameOfficial = "Saint Kitts and Nevis",
        numeric = "659",
        tld = List(".kn")
      )
  case object SaintLucia
      extends Country(
        alpha2 = "LC",
        alpha3 = "LCA",
        name = "Saint Lucia",
        nameOfficial = "Saint Lucia",
        numeric = "662",
        tld = List(".lc")
      )
  case object SaintMartin
      extends Country(
        alpha2 = "MF",
        alpha3 = "MAF",
        name = "Saint Martin (French part)",
        nameOfficial = "The Collectivity of Saint-Martin",
        numeric = "663",
        tld = List(".mf")
      )
  case object SaintPierreAndMiquelon
      extends Country(
        alpha2 = "PM",
        alpha3 = "SPM",
        name = "Saint Pierre and Miquelon",
        nameOfficial = "The Overseas Collectivity of Saint-Pierre and Miquelon",
        numeric = "666",
        tld = List(".pm")
      )
  case object SaintVincentGrenadines
      extends Country(
        alpha2 = "VC",
        alpha3 = "VCT",
        name = "Saint Vincent and the Grenadines",
        nameOfficial = "Saint Vincent and the Grenadines",
        numeric = "670",
        tld = List(".vc")
      )
  case object Samoa
      extends Country(
        alpha2 = "WS",
        alpha3 = "WSM",
        name = "Samoa",
        nameOfficial = "The Independent State of Samoa",
        numeric = "882",
        tld = List(".ws")
      )
  case object SanMarino
      extends Country(
        alpha2 = "SM",
        alpha3 = "SMR",
        name = "San Marino",
        nameOfficial = "The Republic of San Marino",
        numeric = "674",
        tld = List(".sm")
      )
  case object SaoTomeAndPrincipe
      extends Country(
        alpha2 = "ST",
        alpha3 = "STP",
        name = "Sao Tome and Principe",
        nameOfficial = "The Democratic Republic of São Tomé and Príncipe",
        numeric = "678",
        tld = List(".st")
      )
  case object SaudiArabia
      extends Country(
        alpha2 = "SA",
        alpha3 = "SAU",
        name = "Saudi Arabia",
        nameOfficial = "The Kingdom of Saudi Arabia",
        numeric = "682",
        tld = List(".sa")
      )
  case object Senegal
      extends Country(
        alpha2 = "SN",
        alpha3 = "SEN",
        name = "Senegal",
        nameOfficial = "The Republic of Senegal",
        numeric = "686",
        tld = List(".sn")
      )
  case object Serbia
      extends Country(
        alpha2 = "RS",
        alpha3 = "SRB",
        name = "Serbia",
        nameOfficial = "The Republic of Serbia",
        numeric = "688",
        tld = List(".rs")
      )
  case object Seychelles
      extends Country(
        alpha2 = "SC",
        alpha3 = "SYC",
        name = "Seychelles",
        nameOfficial = "The Republic of Seychelles",
        numeric = "690",
        tld = List(".sc")
      )
  case object SierraLeone
      extends Country(
        alpha2 = "SL",
        alpha3 = "SLE",
        name = "Sierra Leone",
        nameOfficial = "The Republic of Sierra Leone",
        numeric = "694",
        tld = List(".sl")
      )
  case object Singapore
      extends Country(
        alpha2 = "SG",
        alpha3 = "SGP",
        name = "Singapore",
        nameOfficial = "The Republic of Singapore",
        numeric = "702",
        tld = List(".sg")
      )
  case object SintMaarten
      extends Country(
        alpha2 = "SX",
        alpha3 = "SXM",
        name = "Sint Maarten (Dutch part)",
        nameOfficial = "Sint Maarten",
        numeric = "534",
        tld = List(".sx")
      )
  case object Slovakia
      extends Country(
        alpha2 = "SK",
        alpha3 = "SVK",
        name = "Slovakia",
        nameOfficial = "The Slovak Republic",
        numeric = "703",
        tld = List(".sk")
      )
  case object Slovenia
      extends Country(
        alpha2 = "SI",
        alpha3 = "SVN",
        name = "Slovenia",
        nameOfficial = "The Republic of Slovenia",
        numeric = "705",
        tld = List(".si")
      )
  case object SolomonIslands
      extends Country(
        alpha2 = "SB",
        alpha3 = "SLB",
        name = "Solomon Islands",
        nameOfficial = "The Solomon Islands",
        numeric = "090",
        tld = List(".sb")
      )
  case object Somalia
      extends Country(
        alpha2 = "SO",
        alpha3 = "SOM",
        name = "Somalia",
        nameOfficial = "The Federal Republic of Somalia",
        numeric = "706",
        tld = List(".so")
      )
  case object SouthAfrica
      extends Country(
        alpha2 = "ZA",
        alpha3 = "ZAF",
        name = "South Africa",
        nameOfficial = "The Republic of South Africa",
        numeric = "710",
        tld = List(".za")
      )
  case object SouthGeorgiaAndSouthSandwichIslands
      extends Country(
        alpha2 = "GS",
        alpha3 = "SGS",
        name = "South Georgia and the South Sandwich Islands",
        nameOfficial = "South Georgia and the South Sandwich Islands",
        numeric = "239",
        tld = List(".gs")
      )
  case object SouthSudan
      extends Country(
        alpha2 = "SS",
        alpha3 = "SSD",
        name = "South Sudan",
        nameOfficial = "The Republic of South Sudan",
        numeric = "728",
        tld = List(".ss")
      )
  case object Spain
      extends Country(
        alpha2 = "ES",
        alpha3 = "ESP",
        name = "Spain",
        nameOfficial = "The Kingdom of Spain",
        numeric = "724",
        tld = List(".es")
      )
  case object SriLanka
      extends Country(
        alpha2 = "LK",
        alpha3 = "LKA",
        name = "Sri Lanka",
        nameOfficial = "The Democratic Socialist Republic of Sri Lanka",
        numeric = "144",
        tld = List(".lk")
      )
  case object Sudan
      extends Country(
        alpha2 = "SD",
        alpha3 = "SDN",
        name = "Sudan (the)",
        nameOfficial = "The Republic of the Sudan",
        numeric = "729",
        tld = List(".sd")
      )
  case object Suriname
      extends Country(
        alpha2 = "SR",
        alpha3 = "SUR",
        name = "Suriname",
        nameOfficial = "The Republic of Suriname",
        numeric = "740",
        tld = List(".sr")
      )
  case object SvalbardJanMayen
      extends Country(
        alpha2 = "SJ",
        alpha3 = "SJM",
        name = "Svalbard Jan Mayen",
        nameOfficial = "Svalbard and Jan Mayen",
        numeric = "744",
        tld = List.empty
      )
  case object Sweden
      extends Country(
        alpha2 = "SE",
        alpha3 = "SWE",
        name = "Sweden",
        nameOfficial = "The Kingdom of Sweden",
        numeric = "752",
        tld = List(".se")
      )
  case object Switzerland
      extends Country(
        alpha2 = "CH",
        alpha3 = "CHE",
        name = "Switzerland",
        nameOfficial = "The Swiss Confederation",
        numeric = "756",
        tld = List(".ch")
      )
  case object SyrianArabRepublic
      extends Country(
        alpha2 = "SY",
        alpha3 = "SYR",
        name = "Syrian Arab Republic (the)",
        nameOfficial = "The Syrian Arab Republic",
        numeric = "760",
        tld = List(".sy")
      )
  case object Taiwan
      extends Country(
        alpha2 = "TW",
        alpha3 = "TWN",
        name = "Taiwan (Province of China)",
        nameOfficial = "The Republic of China",
        numeric = "158",
        tld = List(".tw")
      )
  case object Tajikistan
      extends Country(
        alpha2 = "TJ",
        alpha3 = "TJK",
        name = "Tajikistan",
        nameOfficial = "The Republic of Tajikistan",
        numeric = "762",
        tld = List(".tj")
      )
  case object Tanzania
      extends Country(
        alpha2 = "TZ",
        alpha3 = "TZA",
        name = "Tanzania, the United Republic of",
        nameOfficial = "The United Republic of Tanzania",
        numeric = "834",
        tld = List(".tz")
      )
  case object Thailand
      extends Country(
        alpha2 = "TH",
        alpha3 = "THA",
        name = "Thailand",
        nameOfficial = "The Kingdom of Thailand",
        numeric = "764",
        tld = List(".th")
      )
  case object TimorLeste
      extends Country(
        alpha2 = "TL",
        alpha3 = "TLS",
        name = "Timor-Leste",
        nameOfficial = "The Democratic Republic of Timor-Leste",
        numeric = "626",
        tld = List(".tl")
      )
  case object Togo
      extends Country(
        alpha2 = "TG",
        alpha3 = "TGO",
        name = "Togo",
        nameOfficial = "The Togolese Republic",
        numeric = "768",
        tld = List(".tg")
      )
  case object Tokelau
      extends Country(
        alpha2 = "TK",
        alpha3 = "TKL",
        name = "Tokelau",
        nameOfficial = "Tokelau",
        numeric = "772",
        tld = List(".tk")
      )
  case object Tonga
      extends Country(
        alpha2 = "TO",
        alpha3 = "TON",
        name = "Tonga",
        nameOfficial = "The Kingdom of Tonga",
        numeric = "776",
        tld = List(".to")
      )
  case object TrinidadAndTobago
      extends Country(
        alpha2 = "TT",
        alpha3 = "TTO",
        name = "Trinidad and Tobago",
        nameOfficial = "The Republic of Trinidad and Tobago",
        numeric = "780",
        tld = List(".tt")
      )
  case object Tunisia
      extends Country(
        alpha2 = "TN",
        alpha3 = "TUN",
        name = "Tunisia",
        nameOfficial = "The Republic of Tunisia",
        numeric = "788",
        tld = List(".tn")
      )
  case object Turkey
      extends Country(
        alpha2 = "TR",
        alpha3 = "TUR",
        name = "Turkey",
        nameOfficial = "The Republic of Turkey",
        numeric = "792",
        tld = List(".tr")
      )
  case object Turkmenistan
      extends Country(
        alpha2 = "TM",
        alpha3 = "TKM",
        name = "Turkmenistan",
        nameOfficial = "Turkmenistan",
        numeric = "795",
        tld = List(".tm")
      )
  case object TurksAndCaicosIslands
      extends Country(
        alpha2 = "TC",
        alpha3 = "TCA",
        name = "Turks and Caicos Islands (the)",
        nameOfficial = "The Turks and Caicos Islands",
        numeric = "796",
        tld = List(".tc")
      )
  case object Tuvalu
      extends Country(
        alpha2 = "TV",
        alpha3 = "TUV",
        name = "Tuvalu",
        nameOfficial = "Tuvalu",
        numeric = "798",
        tld = List(".tv")
      )
  case object Uganda
      extends Country(
        alpha2 = "UG",
        alpha3 = "UGA",
        name = "Uganda",
        nameOfficial = "The Republic of Uganda",
        numeric = "800",
        tld = List(".ug")
      )
  case object Ukraine
      extends Country(
        alpha2 = "UA",
        alpha3 = "UKR",
        name = "Ukraine",
        nameOfficial = "Ukraine",
        numeric = "804",
        tld = List(".ua")
      )
  case object UnitedArabEmirates
      extends Country(
        alpha2 = "AE",
        alpha3 = "ARE",
        name = "United Arab Emirates (the)",
        nameOfficial = "The United Arab Emirates",
        numeric = "784",
        tld = List(".ae")
      )
  case object UnitedKingdom
      extends Country(
        alpha2 = "GB",
        alpha3 = "GBR",
        name = "United Kingdom of Great Britain and Northern Ireland (the)",
        nameOfficial = "The United Kingdom of Great Britain and Northern Ireland",
        numeric = "826",
        tld = List(".gb", ".uk")
      )
  case object UnitedStatesMinorOutlyingIslands
      extends Country(
        alpha2 = "UM",
        alpha3 = "UMI",
        name = "United States Minor Outlying Islands (the)",
        nameOfficial =
          "Baker Island, Howland Island, Jarvis Island, Johnston Atoll, Kingman Reef, Midway Atoll, Navassa Island, Palmyra Atoll, and Wake Island",
        numeric = "581",
        tld = List.empty
      )
  case object UnitedStatesOfAmerica
      extends Country(
        alpha2 = "US",
        alpha3 = "USA",
        name = "United States of America (the)",
        nameOfficial = "The United States of America",
        numeric = "840",
        tld = List(".us")
      )
  case object Uruguay
      extends Country(
        alpha2 = "UY",
        alpha3 = "URY",
        name = "Uruguay",
        nameOfficial = "The Oriental Republic of Uruguay",
        numeric = "858",
        tld = List(".uy")
      )
  case object Uzbekistan
      extends Country(
        alpha2 = "UZ",
        alpha3 = "UZB",
        name = "Uzbekistan",
        nameOfficial = "The Republic of Uzbekistan",
        numeric = "860",
        tld = List(".uz")
      )
  case object Vanuatu
      extends Country(
        alpha2 = "VU",
        alpha3 = "VUT",
        name = "Vanuatu",
        nameOfficial = "The Republic of Vanuatu",
        numeric = "548",
        tld = List(".vu")
      )
  case object Venezuela
      extends Country(
        alpha2 = "VE",
        alpha3 = "VEN",
        name = "Venezuela (Bolivarian Republic of)",
        nameOfficial = "The Bolivarian Republic of Venezuela",
        numeric = "862",
        tld = List(".ve")
      )
  case object VietNam
      extends Country(
        alpha2 = "VN",
        alpha3 = "VNM",
        name = "Viet Nam",
        nameOfficial = "The Socialist Republic of Viet Nam",
        numeric = "704",
        tld = List(".vn")
      )
  case object VirginIslandsBritish
      extends Country(
        alpha2 = "VG",
        alpha3 = "VGB",
        name = "Virgin Islands (British)",
        nameOfficial = "The Virgin Islands",
        numeric = "092",
        tld = List(".vg")
      )
  case object VirginIslandsUS
      extends Country(
        alpha2 = "VI",
        alpha3 = "VIR",
        name = "Virgin Islands (U.S.)",
        nameOfficial = "The Virgin Islands of the United States",
        numeric = "850",
        tld = List(".vi")
      )
  case object WallisAndFutuna
      extends Country(
        alpha2 = "WF",
        alpha3 = "WLF",
        name = "Wallis and Futuna",
        nameOfficial = "The Territory of the Wallis and Futuna Islands",
        numeric = "876",
        tld = List(".wf")
      )
  case object WesternSahara
      extends Country(
        alpha2 = "EH",
        alpha3 = "ESH",
        name = "Western Sahara",
        nameOfficial = "The Sahrawi Arab Democratic Republic",
        numeric = "732",
        tld = List.empty
      )
  case object Yemen
      extends Country(
        alpha2 = "YE",
        alpha3 = "YEM",
        name = "Yemen",
        nameOfficial = "The Republic of Yemen",
        numeric = "887",
        tld = List(".ye")
      )
  case object Zambia
      extends Country(
        alpha2 = "ZM",
        alpha3 = "ZMB",
        name = "Zambia",
        nameOfficial = "The Republic of Zambia",
        numeric = "894",
        tld = List(".zm")
      )
  case object Zimbabwe
      extends Country(
        alpha2 = "ZW",
        alpha3 = "ZWE",
        name = "Zimbabwe",
        nameOfficial = "The Republic of Zimbabwe",
        numeric = "716",
        tld = List(".zw")
      )
}
