/*
 * Copyright (c) 2020 Wegtam GmbH
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.countries

import eu.timepit.refined.auto._

import org.scalacheck.{ Arbitrary, Gen }

object CountryGenerators {

  val genCountry: Gen[Country]         = Gen.oneOf(Country.values)
  val genCountries: Gen[List[Country]] = Gen.nonEmptyListOf(genCountry)

  implicit val arbitraryCountry: Arbitrary[Country] = Arbitrary(genCountry)

  implicit val arbitraryCountries: Arbitrary[List[Country]] = Arbitrary(genCountries)

  val invalidAlpha2CountryCode: Alpha2CountryCode   = "XX"
  val invalidAlpha3CountryCode: Alpha3CountryCode   = "XXX"
  val invalidNumericCountryCode: NumericCountryCode = "000"

  val genAlpha2Code: Gen[Alpha2CountryCode] =
    Gen
      .listOfN(2, Gen.alphaChar)
      .map(_.mkString)
      .map(s => Alpha2CountryCode.from(s).toOption.getOrElse(invalidAlpha2CountryCode))

  val genAlpha3Code: Gen[Alpha3CountryCode] =
    Gen
      .listOfN(3, Gen.alphaChar)
      .map(_.mkString)
      .map(s => Alpha3CountryCode.from(s).toOption.getOrElse(invalidAlpha3CountryCode))

  val genNumericCode: Gen[NumericCountryCode] =
    Gen
      .listOfN(3, Gen.numChar)
      .map(_.mkString)
      .map(s => NumericCountryCode.from(s).toOption.getOrElse(invalidNumericCountryCode))

  val validAlpha2Codes: List[Alpha2CountryCode]   = Country.values.map(_.alpha2).toList
  val validAlpha3Codes: List[Alpha3CountryCode]   = Country.values.map(_.alpha3).toList
  val validNumericCodes: List[NumericCountryCode] = Country.values.map(_.numeric).toList
}
