/*
 * Copyright (c) 2020 Wegtam GmbH
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.countries

import com.wegtam.countries.CountryGenerators._
import eu.timepit.refined.auto._

import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.matchers.must.Matchers
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class CountryTest extends AnyWordSpec with Matchers with ScalaCheckPropertyChecks {

  "Country#fromAlpha2" when {
    "the code is found" must {
      "return the country" in {
        forAll("country") { c: Country =>
          Country.fromAlpha2(c.alpha2) must be(Right(c))
        }
      }
    }

    "the code is not found" must {
      "return an error" in {
        forAll((genAlpha2Code, "alpha-2 code")) { a: Alpha2CountryCode =>
          whenever(validAlpha2Codes.find(_ === a).isEmpty) {
            Country.fromAlpha2(a) must be(Left("No such country!"))
          }
        }
      }
    }
  }

  "Country#fromAlpha3" when {
    "the code is found" must {
      "return the country" in {
        forAll("country") { c: Country =>
          Country.fromAlpha3(c.alpha3) must be(Right(c))
        }
      }
    }

    "the code is not found" must {
      "return an error" in {
        forAll((genAlpha3Code, "alpha-3 code")) { a: Alpha3CountryCode =>
          whenever(validAlpha3Codes.find(_ === a).isEmpty) {
            Country.fromAlpha3(a) must be(Left("No such country!"))
          }
        }
      }
    }
  }

  "Country#fromNumeric" when {
    "the code is found" must {
      "return the country" in {
        forAll("country") { c: Country =>
          Country.fromNumeric(c.numeric) must be(Right(c))
        }
      }
    }

    "the code is not found" must {
      "return an error" in {
        forAll((genNumericCode, "numeric code")) { n: NumericCountryCode =>
          whenever(validNumericCodes.find(_ === n).isEmpty) {
            Country.fromNumeric(n) must be(Left("No such country!"))
          }
        }
      }
    }
  }

  "Country#ordering" must {
    "sort countries by name" in {
      forAll("countries") { cs: List[Country] =>
        val expectedList = cs.sortWith((a, b) => a.name.compareTo(b.name) < 1)
        cs.sorted must be(expectedList)
      }
    }
  }

}
