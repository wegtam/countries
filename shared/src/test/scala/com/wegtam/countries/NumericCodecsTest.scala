/*
 * Copyright (c) 2020 Wegtam GmbH
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.countries

import com.wegtam.countries.CountryGenerators._
import com.wegtam.countries.codecs.circe.numeric._
import eu.timepit.refined.auto._
import io.circe.parser._
import io.circe.refined._
import io.circe.syntax._

import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.matchers.must.Matchers
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class NumericCodecsTest extends AnyWordSpec with Matchers with ScalaCheckPropertyChecks {

  "decode" when {
    "input is invalid" must {
      "return an error" in {
        forAll((genNumericCode, "input")) { n: NumericCountryCode =>
          whenever(validNumericCodes.find(_ === n).isEmpty) {
            decode[Country](n.asJson.noSpaces).isLeft must be(true)
          }
        }
      }
    }

    "input is valid" must {
      "return the correct country" in {
        forAll("country") { c: Country =>
          decode[Country](c.numeric.asJson.noSpaces) must be(Right(c))
        }
      }
    }
  }

  "encode" must {
    "produce correct alpha-2 codes" in {
      forAll("country") { c: Country =>
        c.asJson.noSpaces must be(c.numeric.asJson.noSpaces)
      }
    }
  }

}
