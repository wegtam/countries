/*
 * Copyright (c) 2020 Wegtam GmbH
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.countries

import com.wegtam.countries.CountryGenerators._
import com.wegtam.countries.codecs.circe.alpha2._
import eu.timepit.refined.auto._
import io.circe.parser._
import io.circe.refined._
import io.circe.syntax._

import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.matchers.must.Matchers
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class Alpha2CodecsTest extends AnyWordSpec with Matchers with ScalaCheckPropertyChecks {

  "decode" when {
    "input is invalid" must {
      "return an error" in {
        forAll((genAlpha2Code, "input")) { a: Alpha2CountryCode =>
          whenever(validAlpha2Codes.find(_ === a).isEmpty) {
            decode[Country](a.asJson.noSpaces).isLeft must be(true)
          }
        }
      }
    }

    "input is valid" must {
      "return the correct country" in {
        forAll("country") { c: Country =>
          decode[Country](c.alpha2.asJson.noSpaces) must be(Right(c))
        }
      }
    }
  }

  "encode" must {
    "produce correct alpha-2 codes" in {
      forAll("country") { c: Country =>
        c.asJson.noSpaces must be(c.alpha2.asJson.noSpaces)
      }
    }
  }

}
