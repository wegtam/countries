# ChangeLog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## Conventions when editing this file.

Please follow the listed conventions when editing this file:

- one subsection per version
- reverse chronological order (latest entry on top)
- write all dates in iso notation (`YYYY-MM-DD`)
- each version should group changes according to their impact:
    - `Added` for new features.
    - `Changed` for changes in existing functionality.
    - `Deprecated` for once-stable features removed in upcoming releases.
    - `Removed` for deprecated features removed in this release.
    - `Fixed` for any bug fixes.
    - `Security` to invite users to upgrade in case of vulnerabilities.

## Unreleased

## 1.0.0 (2021-02-11)

- no changes from 0.4.0

## 0.4.0 (2021-02-10)

### Added

- support for Scala.js
- scalafix

### Fixed

- "Repository for publishing is not specified." error on `+publishSigned`

## 0.3.0 (2021-02-09)

### Changed

- switch publishing to sonatype
- update Scala-2.12 to 2.12.13
- update Scala-2.13 to 2.13.4
- update cats to 2.3.1
- update refined to 0.9.20

### Removed

- hard dependencies on cats, circe, enumeratum and refined, have to be provided

## 0.2.0 (2020-07-07)

- update Scala-2.12 to 2.12.11

### Removed

- compiler flags for Scala 2.11
- compiler flag to target JVM-1.8 for Scala 2.12

## 0.1.1 (2020-06-29)

- update Scala to 2.13.3

## 0.1.0 (2020-06-23)

- initial release

